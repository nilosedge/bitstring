#include <stdlib.h>
#include <iostream>
#include <unistd.h>

using namespace std;

typedef unsigned long int Uint;

int main() {

	//Uint size = 100000000; // use this to get 6,400,000,000 primes
	Uint size = 20000000; // use this to get    640,000,000 primes
	//Uint size = 1000000; //                      64,000,000
	//Uint size = 100000; //                        6,400,000
	//Uint size = 10000; //                           640,000


	Uint *data = new Uint[size];
	Uint bw = 64;

	for(Uint q = 0; q < size; q++) {
		data[q] = ~0;
	}

//	cout << "Data[i]: " << data[0] << endl;
//	data[0] ^= (Uint)((Uint)1 << (bw - 1));
//	cout << "Data[i]: " << data[0] << endl;

//	return 0;
	Uint modval = 0;
	Uint location = 0;
	Uint i = 0;
	Uint k = 0;
	Uint mv = 0;
	Uint lc = 0;
	Uint str = 0;

	Uint lastP = 0;
	Uint llastP = 0;

	for(i = 2; i < size*bw; i++) {

		modval = (i % bw) + 1;
		location = (Uint)(i/bw);
//		cout << "Mod: " << modval << endl;
//		cout << "Loc: " << location << endl;
//		cout << "I: " << i << endl;
//		cout << "Data[i]: " << data[location] << endl;

		if(data[location] & (Uint)((Uint)1 << (bw - modval))) {

			cout << "Prime: " << i << endl;
			//cout << "Diff : " << i - llastP << endl;
			llastP = lastP;
			lastP = i;


			k = (i*i);
			while(k < size*bw) {
				mv = (k % bw) + 1;
				lc = (Uint)(k/bw);
				str = (Uint)((Uint)1 << (bw - mv));

				//if((k % size) == 0) cout << "Removing: " << k << endl;
				//cout << "Mod: " << mv << endl;
				//cout << "Loc: " << lc << endl;
				//cout << "K: " << k << endl;
				//cout << "ModShift: " << str << endl;
				data[lc] &= ~str;
				//if(data[lc] & str) {
				//	data[lc] -= str;
				//}
				//cout << "Data[i]: " << data[lc] << endl;
				k += i;
			}
		}

		//cout << "DataAfter[i]: " << data[0] << endl;

	}

}
