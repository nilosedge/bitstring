#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <gmp.h>
#include <math.h>

using namespace std;

mpz_t check;
int callcount = 0;
int scancallcount = 0;
unsigned int bitsize = 26;
bool runningdebug = false;

string *values = new string[26];
unsigned int res1a[26];
unsigned int res2a[26];
unsigned int res3a[26];
unsigned int res4a[26];
unsigned int scan1[26];
unsigned int scan2[26];
unsigned int scan3[26];
unsigned int scan4[26];

int scan1dep = 5;
int scan2dep = 7;

void print_found(mpz_t i1, mpz_t i2, unsigned int bitdep, int number) {
}

unsigned int compare_size(mpz_t i1, mpz_t i2, unsigned int bitdep) {
	mpz_t res1;
	mpz_mul(res1, i1, i2);
	unsigned int ret = 0;
	if(mpz_cmp(res1, check) == 0) { 
		print_found(i1, i2, bitdep, 1);
		cout << "WE FOUND IT " << ": " << i1 << " " << i2 << " Call Count: " << callcount << " ScanCallCount: " << scancallcount << " Bitdep: " << bitdep << endl;
		exit(0);
	}
	for(ret = 0; mpz_tstbit(check, ret) == mpz_tstbit(res1, ret); ret++);
	mpz_clear(res1);
	return ret;
}


void factor(mpz_t i1, mpz_t i2, unsigned int bitdep) {
	//if(mpz_cmp(i1, check) > 0 || mpz_cmp(i2, check) > 0) return;
	callcount++;
	if(bitdep == bitsize) return;
	//cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;

	if(callcount % 1000000 == 0) { 
		cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;
		cout << "Counter: " << callcount << endl;
		cout << "Depth: " << bitdep << endl;
	}

	unsigned int res1c, res2c, res3c, res4c;
	res1c = res2c = res3c = res4c = 0;


	mpz_setbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	//cout << "Scan Starting\n";
	//res1c = scanahead(i1, i2, bitdep + 1, scan1dep);
	//cout << "Scan Finished\n";

	mpz_setbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	//res2c = scanahead(i1, i2, bitdep + 1, scan1dep);

	mpz_clrbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	//res3c = scanahead(i1, i2, bitdep + 1, scan1dep);

	mpz_clrbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	//res4c = scanahead(i1, i2, bitdep + 1, scan1dep);


	//cout << "Res1: " << res1c << " Res2: " << res2c << " Res3: " << res3c << " Res4: " << res4c << " BitDep: " << bitdep << endl;
	res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;

	if(res1c > res2c && res1c > res3c && res4c > res2c && res4c > res3c && res1c > bitdep && res4c > bitdep) {
		if(scan2dep > 0 && res1c == res4c) {
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			//res1c = scanahead(i1, i2, bitdep + 1, scan2dep);
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			//res4c = scanahead(i1, i2, bitdep + 1, scan2dep);
			res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;
		}

		if(res1c > res4c) {
			if(runningdebug) cout << "Running :1\n";
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "1a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :4\n";
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "4b";
			factor(i1, i2, bitdep+1);
		} else {
			if(runningdebug) cout << "Running :4\n";
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "4a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :1\n";
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "1b";
			factor(i1, i2, bitdep+1);
		}
	}
	else if(res2c > res1c && res2c > res4c && res3c > res4c && res3c > res1c && res3c > bitdep && res2c > bitdep) {
		if(scan2dep > 0 && res2c == res3c) {
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			//res2c = scanahead(i1, i2, bitdep + 1, scan2dep);
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			//res3c = scanahead(i1, i2, bitdep + 1, scan2dep);
			res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;
		}

		if(res2c > res3c) {
			if(runningdebug) cout << "Running :2\n";
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "2a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :3\n";
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "3b";
			factor(i1, i2, bitdep+1);
		} else {
			if(runningdebug) cout << "Running :3\n";
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "3a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :2\n";
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "2b";
			factor(i1, i2, bitdep+1);
		}
	}
	//cout << "Res1: " << res1c << " Res2: " << res2c << " Res3: " << res3c << " Res4: " << res4c << " BitDep: " << bitdep << endl;
}

int main() {
	mpz_t i1, i2;
	unsigned int bitdep = 1;
	mpz_init_set_ui(i1, 1);
	mpz_init_set_ui(i2, 1);
	//mpz_init_set_str (check, "74037563479561712828046796097429573142593188889231289084936232638972765034028266276891996419625117843995894330502127585370118968098286733173273108930900552505116877063299072396380786710086096962537934650563796359", 10); // bits 352
	//mpz_init_set_str (check, "1763998488000299", 10); // 41999977 41999987 bits26 
	mpz_init_set_str (check, "359985600119", 10); //
	//mpz_init_set_str (check, "284762598577", 10); //
	factor(i1, i2, bitdep);
	cout << "Call Count: " << callcount << endl;
}
