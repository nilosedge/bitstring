#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <gmp.h>
         
using namespace std;
         
typedef unsigned long int Uint;
         
int counter;
mpz_t testnum;
         
bool docalc(mpz_t i1, mpz_t i2, mpz_t tens, mpz_t check) {

   counter++;
   //cout << "I1: " << i1 << " I2: " << i2 << " T: " << tens << " Check: " << check << endl;
          
   if(counter % 1000000 == 0) {
      cout << "I1: " << i1 << " I2: " << i2 << " T: " << tens << " Check: " << check << endl;
      cout << "Counter: " << counter << endl;
   }        
            
   mpz_t add1, add2, mult1, mult2, mod1, mod2, test, newtens;
   mpz_init(add1);
   mpz_init(add2); 
   mpz_init(mod1);
   mpz_init(mod2);
   mpz_init(mult1);
   mpz_init(mult2);
   mpz_init(test);
   mpz_init(newtens);
         
   mpz_mul_ui(newtens, tens, 10);
/*
   if(mpz_cmp(newtens, testnum) > 0) {
      mpz_clear(add1);
      mpz_clear(add2);
      mpz_clear(mod1);
      mpz_clear(mod2);
      mpz_clear(mult1);
      mpz_clear(mult2);
      mpz_clear(test);
      mpz_clear(newtens);
      return false;
   }
*/

   for(int b = 0; b < 10; b++) {
      for(int a = 0; a < 10; a++) {
               
         mpz_mul_ui(mult1, tens, a);
         mpz_add(add1, mult1, i1);
         mpz_mul_ui(mult2, tens, b);
         mpz_add(add2, mult2, i2);
         //cout << " Add1: " << add1 << " Add2: " << add2 << "T: " << test << endl;
               
         if(mpz_cmp(add1, testnum) > 0 && mpz_cmp(add2, testnum) > 0) {
            mpz_clear(add1);
            mpz_clear(add2);
            mpz_clear(mod1);
            mpz_clear(mod2);
            mpz_clear(mult1);
            mpz_clear(mult2);
            mpz_clear(test);
            mpz_clear(newtens);
				return false;
			}

         mpz_mul(test, add1, add2);
         mpz_mod(mod1, test, newtens);
         mpz_mod(mod2, check, newtens);


         if(mpz_cmp(test, check) == 0 && mpz_cmp(add1, check) != 0 && mpz_cmp(add2, check) != 0) {
            cout << "WE FOUND IT: " << add1 << " " << add2 << endl;
            mpz_clear(add1);
            mpz_clear(add2);
            mpz_clear(mod1);
            mpz_clear(mod2);
            mpz_clear(mult1);
            mpz_clear(mult2);
            mpz_clear(test);
            mpz_clear(newtens);
            return true;
         }

         //cout << "Tens: " << tens << endl;
         //cout << "Test: " << test << " Check: " << check << endl;
         //cout << "Mod1: " << mod1 << " Mod2: " << mod2 << endl;

         if(mpz_cmp(mod1, mod2) == 0) {
            //cout << " Add1: " << add1 << " Add2: " << add2 << "T: " << test << endl;

            bool test2 = docalc(add1, add2, newtens, check);
            if(test2) {
               mpz_clear(add1);
               mpz_clear(add2);
               mpz_clear(mod1);
               mpz_clear(mod2);
               mpz_clear(mult1);
               mpz_clear(mult2);
               mpz_clear(test);
               mpz_clear(newtens);
               return test2;
            }
         }


         /*Uint test = ((a * tens) + i1) * ((b * tens) + i2);
         if(test == check) {
            cout << "WE FOUND IT: " << ((a * tens) + i1) << " " << ((b * tens) + i2) << endl;
            return true;
         }
         if(test % (tens * 10) == (check % (tens * 10))) {
            bool test2 = docalc(((a * tens) + i1), ((b * tens) + i2), tens*10, check);
            if(test2) return test2;
         }*/
      }
   }
   mpz_clear(add1);
   mpz_clear(add2);
   mpz_clear(mod1);
   mpz_clear(mod2);
   mpz_clear(mult1);
   mpz_clear(mult2);
   mpz_clear(test);
   mpz_clear(newtens);
   return false;
}


int main() {

   mpz_t i1, i2, ten, check;

   //mpz_init_set_ui(i1, 7);
   //mpz_init_set_ui(i2, 1);
   //mpz_init_set_ui(ten, 10);
   //mpz_init_set_str (testnum, "100000", 10);
   //mpz_init_set_str (check, "2136656807", 10); // 46187 46261
   
   //mpz_init_set_ui(i1, 7);
   //mpz_init_set_ui(i2, 1);
   //mpz_init_set_ui(ten, 10);
   //mpz_init_set_str (testnum, "100000", 10);
   //mpz_init_set_str (check, "3895562999", 10); // 67129 58031

   //mpz_init_set_ui(i1, 7);
   //mpz_init_set_ui(i2, 7);
   //mpz_init_set_ui(ten, 10);
   //mpz_init_set_str (testnum, "100000000", 10);
   //mpz_init_set_str (check, "1763998488000299", 10); //41999977 41999987

   mpz_init_set_ui(i1, 7);
   mpz_init_set_ui(i2, 1);
   mpz_init_set_ui(ten, 10);
   mpz_init_set_str (testnum, "100000000000000", 10);
   mpz_init_set_str (check, "100000000000880000000001887", 10);

   counter = 0;
   docalc(i1, i2, ten, check);
   cout << "Count: " << counter << endl;
/*
   for(int b = 0; b < 10; b++) {
      for(int a = 0; a < 10; a++) {
         unsigned long test = ((a * 10) + 7) * ((b * 10) + 1);
         if(test % 100 == 7) {
            cout << "A: " << a << " B: " << b << " T: " << test << endl;
         }
      }
   }
   //100000000000880000000001887
*/
}

