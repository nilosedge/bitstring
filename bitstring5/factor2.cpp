#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <gmp.h>
#include <math.h>

using namespace std;

mpz_t check;
int callcount = 0;
int scancallcount = 0;
unsigned int bitsize = 26;
bool runningdebug = false;

string *values = new string[26];
unsigned int res1a[26];
unsigned int res2a[26];
unsigned int res3a[26];
unsigned int res4a[26];
unsigned int scan1[26];
unsigned int scan2[26];
unsigned int scan3[26];
unsigned int scan4[26];

int scan1dep = 5;
int scan2dep = 7;

void print_found(mpz_t i1, mpz_t i2, unsigned int bitdep, int number) {
	for(unsigned int i = 1; i < bitsize; i++) { cout << values[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << res1a[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << res2a[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << res3a[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << res4a[i] << "\t"; } cout << endl;
	cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << scan1[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << scan2[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << scan3[i] << "\t"; } cout << endl;
	for(unsigned int i = 1; i < bitsize; i++) { cout << scan4[i] << "\t"; } cout << endl;
	cout << "WE FOUND IT " << number << ": " << i1 << " " << i2 << " Call Count: " << callcount << " ScanCallCount: " << scancallcount << " Bitdep: " << bitdep << endl;
	exit(0);
}

unsigned int scanahead(mpz_t i1, mpz_t i2, unsigned int bitdep, int scandep) {
	scancallcount++;
	if(bitdep == bitsize) return 0;
	if(scandep == 0) return 0;
	
	
/*
	mpz_t tenpow;
	mpz_t mod1;
	mpz_t mod2;
	mpz_t mult;
	mpz_init(tenpow);
	mpz_init(mod1);
	mpz_init(mod2);
	mpz_init(mult);
	mpz_ui_pow_ui(tenpow, 10, (unsigned int)floor(((double)bitdep/10)*3));
	mpz_setbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	mpz_mul(mult, i1, i2);
	mpz_clrbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	mpz_mod(mod1, check, tenpow);
	mpz_mod(mod2, mult, tenpow);
	//if(mpz_cmp(mod1, mod2) != 0) return;
	cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;
	cout << "Mod1: " << mod1 << " Mod2: " << mod2 << " TenPow: " << tenpow << endl;
	mpz_clear(tenpow);
	mpz_clear(mod1);
	mpz_clear(mod2);
	mpz_clear(mult);
*/



	if(scancallcount % 10000000 == 0) { 
		cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;
		cout << "ScanCounter: " << scancallcount << endl;
		cout << "Depth: " << bitdep << endl;
	}

	mpz_t res1, res2, res3, res4;
	unsigned int res1c, res2c, res3c, res4c;
	unsigned int ret1 = 0;
	unsigned int ret2 = 0;
	res1c = res2c = res3c = res4c = 0;
	scan1[bitdep] = scan2[bitdep] = scan3[bitdep] = scan4[bitdep] = 0;


	mpz_init(res1);
	mpz_setbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	mpz_mul(res1, i1, i2);
	if(mpz_cmp(res1, check) == 0) { print_found(i1, i2, bitdep, 1); }
	for(res1c = 0; mpz_tstbit(check, res1c) == mpz_tstbit(res1, res1c); res1c++) {}
	if(ret2 < res1c) ret2 = res1c;
	if(res1c > bitdep) {
		scan1[bitdep] = scanahead(i1, i2, bitdep + 1, scandep - 1);
		//for(int i = 0; i < bitdep; i++) { cout << "   "; }
		//cout << "Scan1: " << scan1[bitdep] << endl;
		if(ret1 < scan1[bitdep]) ret1 = scan1[bitdep];
	}
	mpz_clear(res1);


	mpz_init(res2);
	mpz_setbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	mpz_mul(res2, i1, i2);
	if(mpz_cmp(res2, check) == 0) { print_found(i1, i2, bitdep, 2); }
	for(res2c = 0; mpz_tstbit(check, res2c) == mpz_tstbit(res2, res2c); res2c++) {}
	if(ret2 < res2c) ret2 = res2c;
	if(res2c > bitdep) {
		scan2[bitdep] = scanahead(i1, i2, bitdep + 1, scandep - 1);
		//for(int i = 0; i < bitdep; i++) { cout << "   "; }
		//cout << "Scan2: " << scan2[bitdep] << endl;
		if(ret1 < scan2[bitdep]) ret1 = scan2[bitdep];
	}
	mpz_clear(res2);


	mpz_init(res3);
	mpz_clrbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	mpz_mul(res3, i1, i2);
	if(mpz_cmp(res3, check) == 0) { print_found(i1, i2, bitdep, 3); }
	for(res3c = 0; mpz_tstbit(check, res3c) == mpz_tstbit(res3, res3c); res3c++) {}
	if(ret2 < res3c) ret2 = res3c;
	if(res3c > bitdep) {
		scan3[bitdep] = scanahead(i1, i2, bitdep + 1, scandep - 1);
		//for(int i = 0; i < bitdep; i++) { cout << "   "; }
		//cout << "Scan3: " << scan3[bitdep] << endl;
		if(ret1 < scan3[bitdep]) ret1 = scan3[bitdep];
	}
	mpz_clear(res3);


	mpz_init(res4);
	mpz_clrbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	mpz_mul(res4, i1, i2);
	if(mpz_cmp(res4, check) == 0) { print_found(i1, i2, bitdep, 4); }
	for(res4c = 0; mpz_tstbit(check, res4c) == mpz_tstbit(res4, res4c); res4c++) {}
	if(ret2 < res4c) ret2 = res4c;
	if(res4c > bitdep) {
		scan4[bitdep] = scanahead(i1, i2, bitdep + 1, scandep - 1);
		//for(int i = 0; i < bitdep; i++) { cout << "   "; }
		//cout << "Scan4: " << scan4[bitdep] << endl;
		if(ret1 < scan4[bitdep]) ret1 = scan4[bitdep];
	}
	mpz_clear(res4);

	//cout << "Returning : " << res1c + res2c + res3c + res4c << endl;
	//return res1c + res2c + res3c + res4c + scan1[bitdep] + scan2[bitdep] + scan3[bitdep] + scan4[bitdep];
	return ret1 + ret2;
	//if(ret1 > ret2) return ret1;
	//else return ret2;

}


void factor(mpz_t i1, mpz_t i2, unsigned int bitdep) {
	//if(mpz_cmp(i1, check) > 0 || mpz_cmp(i2, check) > 0) return;
	callcount++;
	if(bitdep == bitsize) return;
	//cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;

	if(callcount % 1000000 == 0) { 
		cout << "I1: " << i1 << " I2: " << i2 << " Check: " << check << endl;
		cout << "Counter: " << callcount << endl;
		cout << "Depth: " << bitdep << endl;
	}

	unsigned int res1c, res2c, res3c, res4c;
	res1c = res2c = res3c = res4c = 0;


	mpz_setbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	//cout << "Scan Starting\n";
	res1c = scanahead(i1, i2, bitdep + 1, scan1dep);
	//cout << "Scan Finished\n";

	mpz_setbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	res2c = scanahead(i1, i2, bitdep + 1, scan1dep);

	mpz_clrbit(i1, bitdep);
	mpz_setbit(i2, bitdep);
	res3c = scanahead(i1, i2, bitdep + 1, scan1dep);

	mpz_clrbit(i1, bitdep);
	mpz_clrbit(i2, bitdep);
	res4c = scanahead(i1, i2, bitdep + 1, scan1dep);


	//cout << "Res1: " << res1c << " Res2: " << res2c << " Res3: " << res3c << " Res4: " << res4c << " BitDep: " << bitdep << endl;
	res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;

	if(res1c > res2c && res1c > res3c && res4c > res2c && res4c > res3c && res1c > bitdep && res4c > bitdep) {
		if(scan2dep > 0 && res1c == res4c) {
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			res1c = scanahead(i1, i2, bitdep + 1, scan2dep);
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			res4c = scanahead(i1, i2, bitdep + 1, scan2dep);
			res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;
		}

		if(res1c > res4c) {
			if(runningdebug) cout << "Running :1\n";
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "1a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :4\n";
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "4b";
			factor(i1, i2, bitdep+1);
		} else {
			if(runningdebug) cout << "Running :4\n";
			mpz_clrbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "4a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :1\n";
			mpz_setbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "1b";
			factor(i1, i2, bitdep+1);
		}
	}
	else if(res2c > res1c && res2c > res4c && res3c > res4c && res3c > res1c && res3c > bitdep && res2c > bitdep) {
		if(scan2dep > 0 && res2c == res3c) {
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			res2c = scanahead(i1, i2, bitdep + 1, scan2dep);
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			res3c = scanahead(i1, i2, bitdep + 1, scan2dep);
			res1a[bitdep] = res1c; res2a[bitdep] = res2c; res3a[bitdep] = res3c; res4a[bitdep] = res4c;
		}

		if(res2c > res3c) {
			if(runningdebug) cout << "Running :2\n";
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "2a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :3\n";
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "3b";
			factor(i1, i2, bitdep+1);
		} else {
			if(runningdebug) cout << "Running :3\n";
			mpz_clrbit(i1, bitdep);
			mpz_setbit(i2, bitdep);
			values[bitdep] = "3a";
			factor(i1, i2, bitdep+1);
			if(runningdebug) cout << "Running :2\n";
			mpz_setbit(i1, bitdep);
			mpz_clrbit(i2, bitdep);
			values[bitdep] = "2b";
			factor(i1, i2, bitdep+1);
		}
	}
	//cout << "Res1: " << res1c << " Res2: " << res2c << " Res3: " << res3c << " Res4: " << res4c << " BitDep: " << bitdep << endl;
}

int main() {
	mpz_t i1, i2;
	unsigned int bitdep = 1;
	mpz_init_set_ui(i1, 1);
	mpz_init_set_ui(i2, 1);
	//mpz_init_set_str (check, "74037563479561712828046796097429573142593188889231289084936232638972765034028266276891996419625117843995894330502127585370118968098286733173273108930900552505116877063299072396380786710086096962537934650563796359", 10); // bits 352
	//mpz_init_set_str (check, "1763998488000299", 10); // 41999977 41999987 bits26 
	//mpz_init_set_str (check, "359985600119", 10); //
	mpz_init_set_str (check, "25005361", 10); //
	//mpz_init_set_str (check, "284762598577", 10); //
	factor(i1, i2, bitdep);
	cout << "Call Count: " << callcount << endl;
}
