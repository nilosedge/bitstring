#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <gmp.h>
#include <math.h>

using namespace std;
mpz_t check;
unsigned int bitsize = 19;

void print_found(mpz_t i1, mpz_t i2, unsigned int bitdep, int number) {
	cout << "WE FOUND IT " << number << ": " << i1 << " " << i2 << " Bitdep: " << bitdep << endl;
	exit(0);
}

unsigned int scan_ahead(mpz_t in1, mpz_t in2, unsigned int bitdep, unsigned int scandepth) {
	//cout << "Testing: " << check << ": " << in1 << " " << in2 << " Bitdep: " << bitdep << endl;

	unsigned int ret1;
	unsigned int ret2;
	unsigned int ret3;
	unsigned int ret4;
	unsigned int ret = 0;
	mpz_t res1;

	if(scandepth == 0) {
		mpz_init(res1);

		mpz_setbit(in1, bitdep);
		mpz_setbit(in2, bitdep);
		mpz_mul(res1, in1, in2);
		if(mpz_cmp(res1, check) == 0) print_found(in1, in2, bitdep, 1);
		for(ret1 = 0; mpz_tstbit(check, ret1) == mpz_tstbit(res1, ret1); ret1++) {}

		mpz_setbit(in1, bitdep);
		mpz_clrbit(in2, bitdep);
		mpz_mul(res1, in1, in2);
		if(mpz_cmp(res1, check) == 0) print_found(in1, in2, bitdep, 2);
		for(ret2 = 0; mpz_tstbit(check, ret2) == mpz_tstbit(res1, ret2); ret2++) {}

		mpz_clrbit(in1, bitdep);
		mpz_setbit(in2, bitdep);
		mpz_mul(res1, in1, in2);
		if(mpz_cmp(res1, check) == 0) print_found(in1, in2, bitdep, 3);
		for(ret3 = 0; mpz_tstbit(check, ret3) == mpz_tstbit(res1, ret3); ret3++) {}
	
		mpz_clrbit(in1, bitdep);
		mpz_clrbit(in2, bitdep);
		mpz_mul(res1, in1, in2);
		if(mpz_cmp(res1, check) == 0) print_found(in1, in2, bitdep, 4);
		for(ret4 = 0; mpz_tstbit(check, ret4) == mpz_tstbit(res1, ret4); ret4++) {}

		mpz_clear(res1);
		//if(ret1check || ret2check || ret3check || ret4check) return 0;
	} else {
		mpz_setbit(in1, bitdep);
		mpz_setbit(in2, bitdep);
		ret1 = scan_ahead(in1, in2, bitdep+1, scandepth-1);
		mpz_setbit(in1, bitdep);
		mpz_clrbit(in2, bitdep);
		ret2 = scan_ahead(in1, in2, bitdep+1, scandepth-1);
		mpz_clrbit(in1, bitdep);
		mpz_setbit(in2, bitdep);
		ret3 = scan_ahead(in1, in2, bitdep+1, scandepth-1);
		mpz_clrbit(in1, bitdep);
		mpz_clrbit(in2, bitdep);
		ret4 = scan_ahead(in1, in2, bitdep+1, scandepth-1);
	}

	if(ret1 > ret2 && ret1 > ret3 && ret4 > ret2 && ret4 > ret3 && ret1 > bitdep && ret4 > bitdep) {
		if(ret1 == ret4) { 
			ret = ret1;
		} else if(ret1 > ret4) {
			ret = ret1;
		} else {
			ret = ret4;
		}
	} else if(ret2 > ret1 && ret2 > ret4 && ret3 > ret4 && ret3 > ret1 && ret3 > bitdep && ret2 > bitdep) {
		if(ret2 == ret3) {
			ret = ret2;
		} else if(ret2 > ret3) {
			ret = ret2;
		} else {
			ret = ret3;
		}
	} else ret = 0;
	cout << "Returning: " << ret << endl;
	return ret;
}


void factor(mpz_t in1, mpz_t in2, unsigned int bitdep) {
	if(bitdep == bitsize) return;
	unsigned int ret1;
	unsigned int ret2;
	unsigned int ret3;
	unsigned int ret4;
	mpz_setbit(in1, bitdep);
	mpz_setbit(in2, bitdep);
	ret1 = scan_ahead(in1, in2, bitdep+1, 5);
	mpz_setbit(in1, bitdep);
	mpz_clrbit(in2, bitdep);
	ret2 = scan_ahead(in1, in2, bitdep+1, 5);
	mpz_clrbit(in1, bitdep);
	mpz_setbit(in2, bitdep);
	ret3 = scan_ahead(in1, in2, bitdep+1, 5);
	mpz_clrbit(in1, bitdep);
	mpz_clrbit(in2, bitdep);
	ret4 = scan_ahead(in1, in2, bitdep+1, 5);

	if(ret1 > ret2 && ret1 > ret3 && ret4 > ret2 && ret4 > ret3 && ret1 > bitdep && ret4 > bitdep) {
		if(ret1 > ret4) {
			mpz_setbit(in1, bitdep);
			mpz_setbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
			mpz_clrbit(in1, bitdep);
			mpz_clrbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
		} else {
			mpz_clrbit(in1, bitdep);
			mpz_clrbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
			mpz_setbit(in1, bitdep);
			mpz_setbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
		}

		//cout << "ret1 was biggest " << endl;
	} else if(ret2 > ret1 && ret2 > ret4 && ret3 > ret4 && ret3 > ret1 && ret3 > bitdep && ret2 > bitdep) {
		if(ret2 > ret3) {
			mpz_setbit(in1, bitdep);
			mpz_clrbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
			mpz_clrbit(in1, bitdep);
			mpz_setbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
		} else {
			mpz_clrbit(in1, bitdep);
			mpz_setbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
			mpz_setbit(in1, bitdep);
			mpz_clrbit(in2, bitdep);
			factor(in1, in2, bitdep+1);
		}
	}
}

int main() {
	mpz_t i1, i2;
	unsigned int bitdep = 1;
	mpz_init_set_ui(i1, 1);
	mpz_init_set_ui(i2, 1);
	//mpz_init_set_str (check, "74037563479561712828046796097429573142593188889231289084936232638972765034028266276891996419625117843995894330502127585370118968098286733173273108930900552505116877063299072396380786710086096962537934650563796359", 10); // bits 352
	//mpz_init_set_str (check, "1763998488000299", 10); // 41999977 41999987 bits26 
	//mpz_init_set_str (check, "359985600119", 10); //
	mpz_init_set_str (check, "284762598577", 10); //
	factor(i1, i2, bitdep);
	//cout << "Call Count: " << callcount << endl;
}

