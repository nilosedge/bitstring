/*
Copyright (C) 2000,2001 Jens Franke.
This file is part of mpqs4linux, distributed under the terms of the 
GNU General Public Licence and WITHOUT ANY WARRANTY.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.

Modified for VC++ 7.1 on Windows by Brian Gladman in April 2004. 
*/

#include <time.h>
#ifndef _MSC_VER
#include <unistd.h>
#include <sys/times.h>
#else
#include <malloc.h>
#include "getline.h"
#define strcasecmp stricmp
#define valloc(n)    _aligned_malloc((n), 4096)
#define vfree(p)	_aligned_free(p)
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <gmp.h>
#include <limits.h>

#include "if.h"

unsigned long verbose = 0;
static unsigned long used_cols, ncol = 80;
static unsigned long log_clock = 0;

void *xmalloc(size_t size)
{
    char*x;
    if (size == 0)
        return NULL;
    if ((x = malloc(size)) == NULL)
        complain("xmalloc: %m");
    else
        return x;
}

void *xvalloc(size_t size)
{
    char*x;
    if (size == 0)
        return NULL;
    if ((x = valloc(size)) == NULL)
        complain("xvalloc: %m");
    else
        return x;
}

void xvfree(void *x)
{
	vfree(x);
}

void *xcalloc(size_t n, size_t s)
{
    void*p;
    if (n == 0 || s == 0)
        return NULL;
    if ((p = calloc(n, s)) != NULL)
        return p;
    complain("calloc: %m");
}

void *xrealloc(void*x, size_t size)
{
    char*y;
    if (size == 0)
    {
        if (x != NULL)
            free(x);
        return NULL;
    }
    if ((y = realloc(x, size)) == NULL && size != 0)
        complain("xrealloc: %m");
    else
        return y;
}

#ifndef _MSC_VER
void xfscanf(FILE*file, char*fmt_string, ...)
{
    va_list arglist;
    unsigned long nread;
    nread = parse_printf_format(fmt_string, 0, NULL);
    va_start(arglist, fmt_string);
    if (vfscanf(file, fmt_string, arglist) != nread)
        complain("xfscanf: %m");
}
#else
#define xfscanf fscanf
#endif

FILE *xfopen(char*Fn, char*mode)
{
    FILE*x;
    if ((x = fopen(Fn, mode)) == NULL)
        complain("Open: %m");
}

void xfclose(FILE*file)
{
    if (fclose(file))
        complain("fclose: %m");
}

int xfgetc(FILE*str)
{
    int i;
    if ((i = fgetc(str)) != EOF)
        return i;
    complain("fgetc: %m");
}

void xfread(void*ptr, size_t size, size_t nmemb, FILE*stream)
{
    if (fread(ptr, size, nmemb, stream) != nmemb)
        complain("fread: %m\n");
}

void xfwrite(void*ptr, size_t size, size_t nmemb, FILE*stream)
{
    if (fwrite(ptr, size, nmemb, stream) != nmemb)
        complain("fread: %m\n");
}

FILE*logfile = NULL;

#ifdef USE_PVM3
unsigned long call_pvmexit_on_exit = 0;
#endif
void complain(char*fmt, ...)
{
    va_list arglist;
    va_start(arglist, fmt);
    vfprintf(stderr, fmt, arglist);
    if (logfile != NULL)
        vfprintf(logfile, fmt, arglist);
#ifdef USE_PVM3
    if (call_pvmexit_on_exit > 0)
        pvm_exit();
#endif
    exit(1);
}

void Schlendrian(char*fmt, ...)
{
    va_list arglist;
    va_start(arglist, fmt);
    vfprintf(stderr, fmt, arglist);
    if (logfile != NULL)
        vfprintf(logfile, fmt, arglist);
#ifdef USE_PVM3
    if (call_pvmexit_on_exit > 0)
        pvm_exit();
#endif
    abort();
}

void numread(char*arg, unsigned*x)
{
    char*fmt;
    int offs;
    if (strlen(arg) == 0)
    {
        *x = 0;
        return ;
    }
    if (*arg == '0')
    {
        if (strlen(arg) == 1)
        {
            *x = 0;
            return ;
        }
        if (*(arg + 1) == 'x')
        {
            fmt = "%X";
            offs = 2;
        }
        else
        {
            fmt = "%o";
            offs = 1;
        }
    }
    else
    {
        offs = 0;
        fmt = "%u";
    }
    if (sscanf(arg + offs, fmt, x) != 1)
    {
        fprintf(stderr, "Bad integer value for some option!\n");
        Usage();
    }
}

#define NEW_NUMBER -0x10000
void logbook(int l, char*fmt, ...)
{
    if (l == NEW_NUMBER)
    {
        used_cols = 0;
        return ;
    }
    if (l < verbose)
    {
        va_list arglist;
        char*output_str;
        unsigned long sl;
        va_start(arglist, fmt);
        vasprintf(&output_str, fmt, arglist);
        sl = strlen(output_str);
        if (used_cols + sl > ncol)
        {
            fprintf(stderr, "\n");
            if (logfile != NULL)
                fprintf(logfile, "\n");
            used_cols = 0;
        }
        fputs(output_str, stderr);
        if (logfile != NULL)
            fputs(output_str, logfile);
        if (output_str[sl - 1] == '\n')
            used_cols = 0;
        else
            used_cols += sl;
        free(output_str);
    }
#if 0
    if (verbose > 2 && l == 0)
    {
        unsigned long log_nclock;
        log_nclock = clock();
        if (log_clock != 0)
        {
            char*output_str;
            unsigned long sl;
            asprintf(&output_str, "[%.1f] ",
                     (float)(log_nclock - log_clock) / CLOCKS_PER_SEC);
            sl = strlen(output_str);
            if (used_cols + sl > ncol)
            {
                fprintf(stderr, "\n");
                used_cols = 0;
            }
            fputs(output_str, stderr);
            if (logfile != NULL)
                fputs(output_str, logfile);
            used_cols += sl;
            free(output_str);
        }
        log_clock = log_nclock;
    }
#endif
}

int errprintf(char*fmt, ...)
{
    va_list arglist;
    int res;

    va_start(arglist, fmt);
    if (logfile != NULL)
        vfprintf(logfile, fmt, arglist);
    res = vfprintf(stderr, fmt, arglist);
    return res;
}

void adjust_bufsize(void**buf, unsigned long *alloc, unsigned long req,
                    unsigned long incr, unsigned long item_size)
{
    if (req > *alloc)
    {
        unsigned long new_alloc;
        new_alloc = *alloc + incr * ((req + incr - 1 - *alloc) / incr);
        if (*alloc > 0)
            *buf = xrealloc(*buf, new_alloc * item_size);
        else
            *buf = xmalloc(new_alloc * item_size);
        *alloc = new_alloc;
    }
}

int yn_query(char*fmt, ...)
{
    va_list arglist;
    char answer[10];

    va_start(arglist, fmt);
    if (logfile != NULL)
        vfprintf(logfile, fmt, arglist);
    vfprintf(stderr, fmt, arglist);
#ifndef _MSC_VER
    if (!isatty(STDIN_FILENO) || !isatty(STDERR_FILENO))
        return 0;
#else
    if (!isatty(_fileno(stdin)) || !isatty(_fileno(stderr)))
        return 0;
#endif
    fflush(stderr);
    while (scanf("%9s", answer) != 1 ||
            (strcasecmp(answer, "yes") != 0 && strcasecmp(answer, "no") != 0))
    {
        fprintf(stderr, "Please answer yes or no!\n");
        vfprintf(stderr, fmt, arglist);
    }
    if (strcasecmp(answer, "yes") == 0)
        return 1;
    return 0;
}

#ifdef __BYTE_ORDER
#ifdef __BIG_ENDIAN
#if __BYTE_ORDER == __BIG_ENDIAN
#include <byteswap.h>

int write_sll(FILE*ofile, long long int*buffer, size_t count)
{
    unsigned long i;
    int res;

    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    res = scatter_write(buffer, sizeof(*buffer), count, ofile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    return res;
}

int write_ull(FILE*ofile, unsigned long long*buffer, size_t count)
{
    unsigned long i;
    int res;

    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    res = scatter_write(buffer, sizeof(*buffer), count, ofile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    return res;
}

int write_sl(FILE*ofile, long int*buffer, size_t count)
{
    unsigned long i;
    int res;

    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    res = scatter_write(buffer, sizeof(*buffer), count, ofile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    return res;
}

int write_ul(FILE*ofile, unsigned long *buffer, size_t count)
{
    unsigned long i;
    int res;

    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    res = scatter_write(buffer, sizeof(*buffer), count, ofile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    return res;
}

int read_sll(FILE*ifile, long long int*buffer, size_t count)
{
    unsigned long i;
    int res;

    res = gather_read(buffer, sizeof(*buffer), count, ifile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    return res;
}

int read_ull(FILE*ifile, unsigned long long*buffer, size_t count)
{
    unsigned long i;
    int res;

    res = gather_read(buffer, sizeof(*buffer), count, ifile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_64(buffer[i]);
    return res;
}

int read_sl(FILE*ifile, long int*buffer, size_t count)
{
    unsigned long i;
    int res;

    res = gather_read(buffer, sizeof(*buffer), count, ifile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    return res;
}

int read_ul(FILE*ifile, unsigned long *, size_t count)
{
    unsigned long i;
    int res;

    res = gather_read(buffer, sizeof(*buffer), count, ifile);
    for (i = 0;i < count;i++)
        buffer[i] = bswap_32(buffer[i]);
    return res;
}

#endif
#endif
#endif

int skip_blanks_comments(char**iline, size_t*iline_alloc, FILE*ifi)
{
    while (getline(iline, iline_alloc, ifi) > 0)
    {
        if (**iline != '#' && strspn(*iline, "\n\t ") < strlen(*iline))
            return 1;
    }
    return 0;
}

int ulong_cmp012(const void*x, const void*y)
{
    const unsigned long *xx, *yy;
    xx = x;
    yy = y;
    if (*xx < *yy)
        return -1;
    if (*xx > *yy)
        return 1;
    return 0;
}

int ulong_cmp210(const void*x, const void*y)
{
    const unsigned long *xx, *yy;
    xx = x;
    yy = y;
    if (*xx < *yy)
        return 1;
    if (*xx > *yy)
        return -1;
    return 0;
}
