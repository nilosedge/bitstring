# Microsoft Developer Studio Project File - Name="ggnfslib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ggnfslib - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ggnfslib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ggnfslib.mak" CFG="ggnfslib - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ggnfslib - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "ggnfslib - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ggnfslib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /I "..\..\include" /I "..\" /Zi /W3 /Od /Op /D "WIN32" /D "_DEBUG" /D "_LIB" /D "_MBCS" /Gm /GZ /c /GX 
# ADD CPP /nologo /MTd /I "..\..\include" /I "..\" /Zi /W3 /Od /Op /D "WIN32" /D "_DEBUG" /D "_LIB" /D "_MBCS" /Gm /GZ /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\bin\ggnfslib.lib" 
# ADD LIB32 /nologo /out:"..\bin\ggnfslib.lib" 
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy ..\..\src\def-par.txt ..\bin\
# End Special Build Tool

!ELSEIF  "$(CFG)" == "ggnfslib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /I "..\..\include" /I "..\" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_LIB" /D "_MBCS" /c /GX 
# ADD CPP /nologo /MT /I "..\..\include" /I "..\" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_LIB" /D "_MBCS" /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\bin\ggnfslib.lib" 
# ADD LIB32 /nologo /out:"..\bin\ggnfslib.lib" 
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy ..\..\src\def-par.txt ..\bin\
# End Special Build Tool

!ENDIF

# Begin Target

# Name "ggnfslib - Win32 Debug"
# Name "ggnfslib - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;def;odl;idl;hpj;bat;asm;asmx"
# Begin Source File

SOURCE=..\..\src\dickman.c
# End Source File
# Begin Source File

SOURCE=..\..\src\ecm4c.c
# End Source File
# Begin Source File

SOURCE=..\..\src\fbgen.c
# End Source File
# Begin Source File

SOURCE=..\..\src\fbmisc.c
# End Source File
# Begin Source File

SOURCE=..\fnmatch.c
# End Source File
# Begin Source File

SOURCE=..\getline.c
# End Source File
# Begin Source File

SOURCE=..\getopt.c
# End Source File
# Begin Source File

SOURCE=..\..\src\getprimes.c
# End Source File
# Begin Source File

SOURCE=..\..\src\llist.c
# End Source File
# Begin Source File

SOURCE=..\..\src\makefb.c
# End Source File
# Begin Source File

SOURCE=..\..\src\misc.c
# End Source File
# Begin Source File

SOURCE=..\modinv1002.c
# End Source File
# Begin Source File

SOURCE=..\..\src\mpz_mat.c
# End Source File
# Begin Source File

SOURCE=..\..\src\mpz_poly.c
# End Source File
# Begin Source File

SOURCE=..\..\src\nfmisc.c
# End Source File
# Begin Source File

SOURCE=..\..\src\poly.c
# End Source File
# Begin Source File

SOURCE=..\..\src\rels.c
# End Source File
# Begin Source File

SOURCE=..\rint.c
# End Source File
# Begin Source File

SOURCE=..\..\src\smintfact.c
# End Source File
# Begin Source File

SOURCE=..\vasprintf.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;inc;xsd"
# Begin Source File

SOURCE=..\..\include-extra\config.h
# End Source File
# Begin Source File

SOURCE=..\..\include\ggnfs.h
# End Source File
# Begin Source File

SOURCE=..\..\include-extra\gmp-impl.h
# End Source File
# Begin Source File

SOURCE=..\..\include-extra\gmp-mparam.h
# End Source File
# Begin Source File

SOURCE=..\..\include-extra\gmp.h
# End Source File
# Begin Source File

SOURCE=..\..\include-extra\longlong.h
# End Source File
# Begin Source File

SOURCE=..\..\include\prand.h
# End Source File
# Begin Source File

SOURCE=..\..\include\version.h
# End Source File
# End Group
# Begin Group "Assembler"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\mulmod32.asm

!IF  "$(CFG)" == "ggnfslib - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "ggnfslib - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# End Group
# Begin Source File

SOURCE=..\..\..\gmp-4.1.4\gmp.build.vc7\lib_gmp\Release\gmp.lib
# End Source File
# End Target
# End Project

