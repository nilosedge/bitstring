# Microsoft Developer Studio Project File - Name="pol51m0b" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=pol51m0b - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "pol51m0b.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "pol51m0b.mak" CFG="pol51m0b - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "pol51m0b - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "pol51m0b - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "pol51m0b - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /I "..\" /ZI /W3 /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "HAVE_ASM_INTEL" /D "_MBCS" /Gm /GZ /c /GX 
# ADD CPP /nologo /MTd /I "..\" /ZI /W3 /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "HAVE_ASM_INTEL" /D "_MBCS" /Gm /GZ /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /out:"Debug\pol51m0b.exe" /incremental:yes /debug /pdb:"Debug\pol51m0b.pdb" /pdbtype:sept /subsystem:console /machine:ix86 
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /out:"Debug\pol51m0b.exe" /incremental:yes /debug /pdb:"Debug\pol51m0b.pdb" /pdbtype:sept /subsystem:console /machine:ix86 

!ELSEIF  "$(CFG)" == "pol51m0b - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /I "..\" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "HAVE_ASM_INTEL" /D "_MBCS" /c /GX 
# ADD CPP /nologo /MT /I "..\" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "HAVE_ASM_INTEL" /D "_MBCS" /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /out:"Release\pol51m0b.exe" /incremental:no /debug /pdbtype:sept /subsystem:console /opt:ref /opt:icf /machine:ix86 
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /out:"Release\pol51m0b.exe" /incremental:no /debug /pdbtype:sept /subsystem:console /opt:ref /opt:icf /machine:ix86 

!ENDIF

# Begin Target

# Name "pol51m0b - Win32 Debug"
# Name "pol51m0b - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;def;odl;idl;hpj;bat;asm;asmx"
# Begin Source File

SOURCE=..\..\src\pol5\if.c
# End Source File
# Begin Source File

SOURCE=..\..\src\pol5\pol51m0b.c
# End Source File
# Begin Source File

SOURCE=..\..\src\pol5\zeit.c
# End Source File
# Begin Group "assembler"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\asm_hash5.asm

!IF  "$(CFG)" == "pol51m0b - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "pol51m0b - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# End Group
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;inc;xsd"
# Begin Source File

SOURCE=..\..\src\pol5\if.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\bin\ggnfslib.lib
# End Source File
# End Target
# End Project

