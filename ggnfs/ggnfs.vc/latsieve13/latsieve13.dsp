# Microsoft Developer Studio Project File - Name="latsieve13" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=latsieve13 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "latsieve13.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "latsieve13.mak" CFG="latsieve13 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "latsieve13 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE "latsieve13 - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /I "..\" /I "..\..\include" /I "..\..\src\lasieve4\piii" /Zi /W3 /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "I_bits=13" /D "_MBCS" /Gm /GZ /c /GX 
# ADD CPP /nologo /MTd /I "..\" /I "..\..\include" /I "..\..\src\lasieve4\piii" /Zi /W3 /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "I_bits=13" /D "_MBCS" /Gm /GZ /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /out:"..\bin\gnfs-lasieve4I13e.exe" /incremental:yes /debug /pdb:"Debug\latsieve13.pdb" /pdbtype:sept /subsystem:console /machine:ix86 
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /out:"..\bin\gnfs-lasieve4I13e.exe" /incremental:yes /debug /pdb:"Debug\latsieve13.pdb" /pdbtype:sept /subsystem:console /machine:ix86 

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /I "..\" /I "..\..\include" /I "..\..\src\lasieve4\piii" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "I_bits=13" /D "_MBCS" /c /GX 
# ADD CPP /nologo /MT /I "..\" /I "..\..\include" /I "..\..\src\lasieve4\piii" /Zi /W3 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "I_bits=13" /D "_MBCS" /c /GX 
# ADD BASE MTL /nologo /win32 
# ADD MTL /nologo /win32 
# ADD BASE RSC /l 1033 
# ADD RSC /l 1033 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo 
# ADD BSC32 /nologo 
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /out:"..\bin\gnfs-lasieve4I13e.exe" /incremental:no /debug /pdbtype:sept /subsystem:console /opt:ref /opt:icf /machine:ix86 
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /out:"..\bin\gnfs-lasieve4I13e.exe" /incremental:no /debug /pdbtype:sept /subsystem:console /opt:ref /opt:icf /machine:ix86 

!ENDIF

# Begin Target

# Name "latsieve13 - Win32 Debug"
# Name "latsieve13 - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\src\lasieve4\fbgen.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\gmp-aux.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\gnfs-lasieve4e.c
# End Source File
# Begin Source File

SOURCE=..\if.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\input-poly.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\lasieve-prepn.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\mpz-ull.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\prho.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\primgen32.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\real-poly-aux.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\recurrence6.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\redu2.c
# End Source File
# Begin Group "piii"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\basemath.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\gcd32.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\lasched.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\medsched.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\montgomery_mul.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\mpqs-hack.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\psp.c
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\siever-config.c
# End Source File
# End Group
# Begin Group "assembler"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\lat_asm\asm_mul128.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=..\lat_asm\asm_mul64.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=..\lat_asm\asm_mul96.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=..\lat_asm\modinv1002.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=..\lat_asm\ri-aux.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# Begin Source File

SOURCE=..\lat_asm\sieve-from-sched.asm

!IF  "$(CFG)" == "latsieve13 - Win32 Debug"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "latsieve13 - Win32 Release"

# PROP Ignore_Default_Tool 1
# Begin Custom Build - 
SOURCE="$(InputPath)"

BuildCmds= \
	nasm -Xvc -O9 -f win32 -o "$(ProjDir)ir)\$(IntDir)\$(InputName).obj" "$(InputPath)" \


""$(ProjDir)ir)\$(IntDir)\$(InputName).obj"" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF

# End Source File
# End Group
# End Group
# Begin Group "Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\src\lasieve4\generic\32bit.h
# End Source File
# Begin Source File

SOURCE=..\if.h
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\piii\lasieve-asm.h
# End Source File
# Begin Source File

SOURCE=..\..\src\lasieve4\lasieve.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\bin\ggnfslib.lib
# End Source File
# End Target
# End Project

