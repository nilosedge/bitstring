/*
Copyright (C) 2000,2001 Jens Franke.
This file is part of mpqs4linux, distributed under the terms of the 
GNU General Public Licence and WITHOUT ANY WARRANTY.

You should have received a copy of the GNU General Public License along
with this program; see the file COPYING.  If not, write to the Free
Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.

Modified for VC++ 7.1 on Windows by Brian Gladman in April 2004. 
*/

#ifdef _MSC_VER
#define __BIG_ENDIAN 1234
#define __LITTLE_ENDIAN 4321
#define __BYTE_ORDER 4321
#endif

#include <stdarg.h>
#include <stdio.h>

void*xmalloc(size_t size);
void*xvalloc(size_t size);
void xvfree(void *x);
void*xcalloc(size_t n, size_t s);
void*xrealloc(void*x, size_t size);
void xfscanf(FILE*file, char*fmt_string, ...);
FILE*xfopen(char*Fn, char*mode);
void xfclose(FILE*file);
int xfgetc(FILE*str);
void xfread(void*ptr, size_t size, size_t nmemb, FILE*stream);
void xfwrite(void*ptr, size_t size, size_t nmemb, FILE*stream);
void complain(char*fmt, ...);
void Schlendrian(char*fmt, ...);
void logbook(int l, char*fmt, ...);
int errprintf(char*fmt, ...);
void numread(char*arg, unsigned*x);
void adjust_bufsize(void**, unsigned long*, unsigned long, unsigned long, unsigned long);
extern unsigned long verbose;
extern FILE*logfile;
#ifdef __BYTE_ORDER
#ifdef __BIG_ENDIAN
#if __BYTE_ORDER == __BIG_ENDIAN
int write_sll(FILE*, long long int*, size_t);
int write_ull(FILE*, unsigned long long*, size_t);
int write_sl(FILE*, long int*, size_t);
int write_ul(FILE*, unsigned long*, size_t);
int read_sll(FILE*, long long int*, size_t);
int read_ull(FILE*, unsigned long long*, size_t);
int read_sl(FILE*, long int*, size_t);
int read_ul(FILE*, unsigned long*, size_t);
#else
#define write_sll(ofile,buffer,count) fwrite(buffer,sizeof(long long int),count,ofile)
#define write_ull(ofile,buffer,count) fwrite(buffer,sizeof(unsigned long long),count,ofile)
#define write_ul(ofile,buffer,count) fwrite(buffer,sizeof(unsigned long),count,ofile)
#define write_sl(ofile,buffer,count) fwrite(buffer,sizeof(long int),count,ofile)
#define read_sll(ofile,buffer,count) fread(buffer,sizeof(long long int),count,ofile)
#define read_ull(ofile,buffer,count) fread(buffer,sizeof(unsigned long long),count,ofile)
#define read_ul(ofile,buffer,count) fread(buffer,sizeof(unsigned long),count,ofile)
#define read_sl(ofile,buffer,count) fread(buffer,sizeof(long int),count,ofile)
#endif
#endif
#endif
#ifdef USE_PVM3
extern unsigned long call_pvmexit_on_exit;
#endif
int yn_query(char*fmt, ...);
int skip_blank_comments(char**, size_t*, FILE*);
int ulong_cmp012(const void*, const void*);
int ulong_cmp210(const void*, const void*);
