#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include <string>

using namespace std;

typedef unsigned long int Uint;

int main() {

	Uint end = 1000000000;
	Uint count = 0;
	Uint count2 = 0;
	Uint val0 = 0;
	Uint val1 = 0;

   Uint modval = 0;
   Uint location = 0;
	Uint oldlocation = 1000;
   Uint mv = 0;
   Uint lc = 0;
	// Uint olc = 0;
   Uint str = 0;

	int intsize = sizeof(val0);
	Uint bitwidth = intsize * 8;

	fstream file ("workfile", ios::in|ios::out);
	if(!file) { cout << "File did not open." << endl; exit(0); }

	for(count = 37; count < (end / 2); count++) {

		modval = (Uint)((count % bitwidth) + 1);
		location = (Uint)(count/bitwidth);
		if(location != oldlocation || count <= bitwidth) {
			file.clear();
			file.seekg(location * intsize);
			file.read((char *)(&val0), intsize);
			//cout << "Location: " << location << endl;
			//cout << "Val: " << val0 << endl;
			oldlocation = location;
		}

		if(((Uint)((Uint)1 << (bitwidth - modval)) & (Uint)val0) == 0) {

			cout << "Prime: " << count << endl;
			count2 = count * count;

			while(count2 < end) {

				mv = (count2 % bitwidth) + 1;
				lc = (Uint)(count2/bitwidth*intsize);
				str = (Uint)((Uint)1 << (bitwidth - mv));

//				if(lc != olc) {
//					//cout << "Does this happen: " << lc << " " << olc << " " << val1 << " " <<  endl;
//					file.seekp(olc * intsize);
//					file.write((char *)(&val1), intsize);
//					file.seekg(lc * intsize);
//					file.read((char *)(&val1), intsize);
//					//cout << "Read in: " << val1 << endl;
//					olc = lc;
//				}

				//cout << "Loc: " << lc << " ValB: " << val1 << endl;
				file.seekg(lc);
				file.read((char *)(&val1), intsize);
				val1 |= str;
				file.seekp(lc);
				file.write((char *)(&val1), intsize);
				//file.flush();
				//cout << "Loc: " << lc << " ValA: " << val1 << endl;

				count2 += count;
			}
		}
	}
}
