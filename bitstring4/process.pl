#!/usr/bin/perl -w

while(<>) {
	if($_ =~ /0b([01]*)/) {
		$bin = $1;
		$int = `echo "ibase=2;$bin" | bc`;
		chomp $int;
		$_ =~ s/0b$bin/$int/;
		print $_;
		
	}
}
