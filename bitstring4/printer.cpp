#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include <string>

using namespace std;
typedef unsigned long int Uint;


int main() {
	Uint val0 = 0;
	int intsize = sizeof(val0);
	int bitwidth = intsize * 8;

	ifstream file ("workfile", ios::in);
	if(!file) { cout << "File did not open." << endl; exit(0); }

	Uint begin = file.tellg();
	file.seekg(0, ios::end);
	Uint end = file.tellg();
	end /= intsize;
//	cout << end << endl;
	file.seekg(0);

	for(Uint count = 0; count < end; count++) {
//		cout << count << endl;
		file.seekg(count * intsize);
		file.read((char *)(&val0), intsize);
//				cout << val0 << endl;

		for(int i = (count * bitwidth); i < (count * bitwidth) + bitwidth; i++) {

			if(!(((Uint)1 << (bitwidth - (i - (count * bitwidth)) - 1)) & val0)) {
				cout << i << endl;
//				cout << val0 << endl;
//				cout << (bitwidth - (i - (count * bitwidth)) - 1) << endl;
			}
		}
	}
}
