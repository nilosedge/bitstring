#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <math.h>
#include <string>

using namespace std;

//typedef mpz_t Uint; 
typedef unsigned long int Uint;

int main() {
	
   Uint val0 = 0;
   int intsize = sizeof(val0);

	fstream file ("workfile", ios::in|ios::out);

   file.seekg(0, ios::end);
   Uint end = file.tellg();
   //end /= intsize;
   file.seekg(0);

   for(Uint count = 0; count < end; count += intsize) {

		file.seekp(count);
		file.write((char *)(&val0), intsize);

	}
}
