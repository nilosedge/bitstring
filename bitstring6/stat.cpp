#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>

typedef double Uint;

using namespace std;

int main() {
	srand(time(0));
	int size = 4;
	Uint target = 25005361;
	Uint **top = new Uint *[size];
	int *topv = new int[size];
	Uint **side = new Uint *[size];
	int *sidev = new int[size];
	
	for(int i = 0; i < size; i++) {
		top[i] = new Uint[10];
		side[i] = new Uint[10];
		for(int k = 0; k < 10; k++) {
			top[i][k] = side[i][k] = 0;
		}
	}

	int counter = 1;
	while(counter > 0) {
		for(int i = 0; i < size; i++) {
			int ran = rand();
			//cout << "Size: " << ran % 10 << endl;
			//cout << "Size: " << (ran / 10) % 10 << endl;
			sidev[i] = (ran % 10);
			topv[i] = ((ran / 10) % 10);
		}
	
		double topcounter = 0;
		double sidecounter = 0;
		for(int i = 0; i < size; i++) {
			topcounter += pow(10,i) * topv[i];
			sidecounter += pow(10,i) * sidev[i];
		}	

		
	
		double total = ((double)(target - (topcounter * sidecounter)) / (double)target * 1000);
		//cout << total << endl;
	
		for(int i = 0; i < size; i++) {
			//cout << total / pow(10, size - i) << endl;
			side[i][sidev[i]] += total;
			top[i][topv[i]] += total;
		}
	
		if(counter % 1000000 == 0) {
			cout << "Side: " << endl;	
			for(int i = size - 1; i >= 0; i--) {
				for(int k = 0; k < 10; k++) {
					cout << (long)side[i][k] << " ";
				}
				cout << endl;
			}
			cout << "Top: " << endl;	
			for(int i = size - 1; i >= 0; i--) {
				for(int k = 0; k < 10; k++) {
					cout << (long)top[i][k] << " ";
				}
				cout << endl;
			}
		}
		counter++;
	}
}
